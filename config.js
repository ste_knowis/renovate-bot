module.exports = {
    platform: 'gitlab',
    onboardingConfig: {
        extends: ['config:base'],
    },
    repositories: [
        "ste_knowis/renovate-npm-reproduction"
    ],

    skipInstalls: false,
    updateLockFiles: true,

    allowedPostUpgradeCommands: [
        "echo",
        "install-tool yarn",
        "yarn install",
        "ls",
        "yarn husky install",
        "./node_modules/.bin/husky install"
    ],

    postUpgradeTasks: {
        "commands": [
            "install-tool yarn 1.22.19",
            "yarn install",
            "ls -lisa",
            "ls node_modules -lisa",
            "yarn husky install",
            "./node_modules/.bin/husky install"
        ],
    },
}
